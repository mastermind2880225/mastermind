import requests as rq
import json
import random 
words = [word.strip() for word in open("5letters.txt")]

mm_url ="https://we6.talentsprint.com/wordle/game/"
register_url = mm_url + "register"

register_dict={"mode" :"Mastermind" , "name" : "radha"}
register_with =json.dumps(register_dict)
r=rq.post(register_url,json=register_dict)
print(r.text)
session=rq.Session()
resp= session.post(register_url, json = register_dict)
me = resp.json() [ 'id']
create_url=mm_url+"create"
create_dict = {'id' :me, "overwrite" :True }
rc=session.post(create_url , json= create_dict)
guess_url = mm_url + "guess"
print(rc.json())


def matching_letters(word1,word2):
    return(len((set(word1)).intersection(set(word2))))

def feedback0(guess, words):
    for word in words:
        if (matching_letters(guess, word)) > 0:
            words.remove(word)
    return words
    
def other_feedback(guess, match_count, words):
    new_words = []
    for word in words:
        if matching_letters(guess, word) == match_count:
            new_words.append(word)
    if guess in new_words:
        new_words.remove(guess)
    return new_words
guess = random.choice(words) 
words = [word for word in words if len(word) == 5]  

while True:
    guess_dict = {'id': me, 'guess': guess}
    guess_resp = session.post(guess_url, json=guess_dict)
    feedback = int(guess_resp.json()['feedback'])
    message = guess_resp.json()['message']
    print(f" Guess = {guess}, Feedback = {feedback}")
    if feedback == 5 and ("win" in message):
        print("word guessed:", guess)
        break
    elif feedback == 0:
        words = feedback0(guess, words)
    else:
        words = other_feedback(guess, feedback, words)
    if words:
        guess = random.choice(words) 
    else:
        print("No more valid words to guess.")
        break


