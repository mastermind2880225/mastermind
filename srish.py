forbidden_letters = ['q', 'w', 'e', 'r', 't', 'y', 'a', 'i', 'o', 'p', 's', 'd']
file_path = '5letters.txt'

# Read words from file
with open(file_path, 'r') as file:
    words = file.read().split()

# Function to check if a word contains any forbidden letters
def contains_forbidden_letters(word, forbidden_letters):
    for letter in forbidden_letters:
        if letter in word:
            return True
    return False

# Collect words that do not contain forbidden letters
valid_words = [word for word in words if not contains_forbidden_letters(word, forbidden_letters)]

# Print the valid words
for word in valid_words:
    print(word)

