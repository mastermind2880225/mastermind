def load_words(fileName):
    with open(fileName, 'r') as file:
        words = file.read().splitlines()
    return words

def matching_letters(word1,word2):
    return(len((set(word1)).intersection(set(word2))))

def 0feedback(guess, words):
    new_words = []
    for word in words:
        if matching_letters(guess, word) == 0:
            new_words.append(word)
    return new_words

def filter_words_for_other_feedback(guess, match_count, word_list):
    new_words = []
    for word in word_list:
        if matching_letters(guess, word) >= match_count:
            new_words.append(word)
    new_words.remove(guess)
    return new_words




